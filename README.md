# Lane Line Detection

In this udacity nano degree assigment I implement a pipeline to detect lanes on images.

## Objective
The objective of this assignment was to implement an image processing pipeline
that is capable of detecting road lanes on images and videos. This was achieved
by using the python OpenCV library. The design of the algorithm and the 
different tools that were used will be described in the following.

## Image Processing
The algorithm processes an image in five major steps:
*  Transform an image to grayscale
*  Apply Gaussian Blurring to reduce noise
*  Find edges through Canny-Edge detection
*  Mask the image to include only region of interest
*  Converting the edges to Hough-lines
*  Weighing the image

## Results


## Potential Shortcomings


